import React, { Component } from 'react';
import './App.css';
import Header from "./components/Header/Header";
import Contacts from "./containers/Contacts/Contacts";
import {Route, Switch} from "react-router-dom";
import About from "./containers/About us/About us";
import AppPost from "./containers/AppPost/AppPost";
import Posts from "./containers/Posts/Posts";
import Post from "./components/Post";


class App extends Component {
  render() {
    return (
      <div className="App">
          <Header/>
          <Switch>
              <Route path="/" exact component={Posts}/>
              <Route path="/about us" component={About}/>
              <Route path="/contacts" component={Contacts}/>
              <Route path="/add post" component={AppPost}/>
              <Route path="/posts/:id" component={Post}/>
          </Switch>
      </div>
    );
  }
}

export default App;

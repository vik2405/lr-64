import React from 'react';
import './Panel.css'


  let Panel = (props)=> {

    return(

<div className='panel'>
      <input type="text" placeholder="TITLE" className="users" name="title" value={props.title} onChange={props.change}/>
        <textarea className='message' name="message" placeholder="Your message" cols="20" rows="2" value={props.message} onChange={props.change}></textarea><button className="send" onClick={props.send}>Send</button>
</div>
        )
        };

      export default Panel;

import React, { Component } from 'react';
import './Posts.css';
import axios from 'axios';
import Fields from "../../Fields/Fields";

class Posts extends Component {
    state = {
        getmessage: {}
    };
    GetMessage = () => {
        axios.get('/message.json').then(response => {
            response.data ?
            this.setState({getmessage:response.data}) : null;
        });
    };
    componentDidMount() {
        this.GetMessage()
    };

    removePost = (id) => {
        axios.delete('/message/' + id + '.json').then(response => {
            this.GetMessage()
        });
    };

    render() {
        return (
            <div className="post">
                <h3>Post User</h3>
                <Fields  messages={this.state.getmessage} remove={this.removePost}  />
            </div>
        );
    }
}

export default Posts;

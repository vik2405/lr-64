import React, { Component } from 'react';
import './AppPost.css';
import axios from 'axios';
import Panel from "../../Panel/Panel";
import Fields from "../../Fields/Fields";

class AppPost extends Component {
    state = {
        message: '',
        title: '',
    };


    GetMessage = () => {
        axios.get('/message.json').then(response => {

            this.setState({getmessage:response.data});
        });
    };


    componentDidMount() {
        this.GetMessage()
    };

    fullchange = (event) => {
        this.setState({[event.target.name]: event.target.value})
    };

    addMessage = () => {
        axios.post('/message.json', { addmessage:this.state.message, title: this.state.title, date: new Date()}).then(() => {
                this.GetMessage();
            }
        )
    };


    render() {
    return (
      <div className="App">
          <h3>Add Post</h3>
          <Panel  change = {this.fullchange} send = {this.addMessage } message = {this.state.message} title={this.state.title}/>
      </div>
    );
  }
}

export default AppPost;

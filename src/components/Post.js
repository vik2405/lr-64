import React, { Component } from 'react';
import axios from 'axios';
import './Post.css';
class Post extends Component {

    state={};

    componentDidMount() {
        axios.get(`/message/${this.props.match.params.id}.json`).
        then(response => {
            this.setState(response.data);

        });
    }
    render() {
        return (
            <div className="newpost">
                 {this.state.title} {this.state.date} {this.state.addmessage}
            </div>
        )
    }
}

export default Post;
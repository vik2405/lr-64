import React from 'react';
import {NavLink} from 'react-router-dom'
import './Header.css';
const Header = () => {
    return (
        <header>
            <div className="Navigation">
            <nav className="nav">
                <ul className="ul">
                    <li><NavLink to="/" exact activeClassName="active">Home</NavLink></li>
                    <li><NavLink to="/About us" activeClassName="active">About us</NavLink></li>
                    <li><NavLink to="/Contacts" activeClassName="active">Contacts</NavLink></li>
                    <li><NavLink to="/Add Post" activeClassName="active">Add Post</NavLink></li>
                </ul>
            </nav>
            </div>
        </header>
    )
};

export default Header;
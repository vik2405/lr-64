import React from 'react';
import './Fields.css'
import Field from "../Field/Field";

let Fields = (props)=> {
    const messages = Object.keys(props.messages);
    return(
        <div className='fields'>
            {messages.map((messageId) => <Field text={props.messages[messageId].addmessage}
                                                title={props.messages[messageId].title}
                                                date={props.messages[messageId].date}
                                                messageId={messageId}
                                                remove={() => props.remove(messageId)} itemId={messageId} key={messageId}
                                                 />)}
        </div>
    )
};

export default Fields;
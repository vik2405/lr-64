import React from 'react';
import './Field.css'
import {Link} from "react-router-dom";

let Field = (props)=> {

    return(
        <div className='field'>
            <h3 id="field">{props.title}</h3>
            <button onClick={props.remove} className="button">DELETE</button>
            <Link to={`/posts/${props.messageId}`} className="read">Read more</Link>
        </div>
    )
};

export default Field;